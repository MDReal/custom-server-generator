const newPackageJSON = require("./f/PackageJSON");
const { resolve, dirname } = require("path");
const cleanDir = require("./f/cleanDir");
const install = require("./f/install");
const server = require("./server");
const client = require("./client");
const createFile = require("./f/createFile");
const createFolder = require("./f/createFolder");
const root = process.cwd();

module.exports = async (app, args) => {
	/* nodemon --watch module --exec "clear && NODE_ENV=development generate-custom-server -s -n -r -e" */

	const pname = app.toLowerCase();

	const { engine, router, electron, socket, client: clientFS, react, next, sass, pug } = args;
	const serverSystem = await server(engine, router, electron, socket, next, pug, clientFS, pname);
	const clientSystem = await client(socket, react, next, sass, pug, clientFS);

	if (process.env.NODE_ENV === "development") cleanDir(resolve(root, pname));

	const package = newPackageJSON(pname, electron);
	const files = { ...clientSystem.files, ...serverSystem.files };
	const folders = [...clientSystem.folders, ...serverSystem.folders];

	createFile(resolve(root, pname, "package.json"), JSON.stringify(package, null, 2));
	await install(resolve(root, pname), false, [...clientSystem.packages, ...serverSystem.packages]);
	await install(resolve(root, pname), true, [
		...clientSystem.devPackages,
		...serverSystem.devPackages,
	]);

	if (clientSystem.react) {
		createFile(
			resolve(root, pname, clientFS, "package.json"),
			JSON.stringify(clientSystem.react.packageJSON, null, 2),
		);

		await install(resolve(root, pname, clientFS), false, clientSystem.react.packages);
		await install(resolve(root, pname, clientFS), true, clientSystem.react.devPackages);
	}

	const config = { ...(clientSystem.config || {}), ...(serverSystem.config || {}) };
	Object.entries(config.js).map(([name, content]) =>
		createFile(`${pname}/${name}.config.js`, content),
	);
	Object.entries(config.json).map(([name, content]) =>
		createFile(`${pname}/${name}.json`, content),
	);
	Object.entries(files).map(([file, content]) => createFile(resolve(root, pname, file), content));
	folders.map((folder) => createFolder(resolve(root, pname, folder)));
};
