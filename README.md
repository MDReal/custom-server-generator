# Custom Server Generator

### Command: generate-custom-server

<pre>

Engines:
  -e, --engine  Engines
		input: String
		choices: ["http", "https", "express"]
		default: "express"

For Client:
  -n, --next   NextJS Support
		input: boolean

  -r, --react  ReactJS Support
		input: boolean

  -p, --pug    PugJS Support
		input: boolean


Technologies:
  -l, --electron  ElectronJS Support
		input: boolean

  -s, --socket    SocketJS Support
		input: boolean

  -a, --sass    NextJS s(a/c)ss Support
		input: boolean


Settings:
  -i, --client  Client Folder
		type: String
		default: "client"

Options:
  --version     Show version number
		input: boolean

  --help        Show help
		input: boolean

  -x, --router  Example Router
		input: boolean

</pre>

## Usage

```Terminal
npm i -g generate-custom-server

generate-custom-server [name] <args>
```
