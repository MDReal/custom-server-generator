#!/usr/bin/env node
const yargs = require("yargs");
const packageJSON = require("../package.json");
const version = packageJSON.version;

require("colors");
require("../f/messages");

const opt = (key, alias, desc, additionals = { type: "boolean" }) => ({
	[key]: {
		alias,
		desc,
		...additionals,
	},
});

const opts = {
	...opt("e", "engine", "Engines", {
		default: "express",
		type: "string",
		choices: ["http", "https", "express"],
	}),
	...opt("x", "router", "Example Router"),
	...opt("n", "next", "NextJS Support"),
	...opt("a", "sass", "NextJS s(a/c)ss Support"),
	...opt("r", "react", "ReactJS Support"),
	...opt("p", "pug", "PugJS Support"),
	...opt("l", "electron", "ElectronJS Support"),
	...opt("s", "socket", "SocketJS Support"),
	...opt("i", "client", "Client Folder", { type: "string", default: "client" }),
};

args = yargs
	.usage(
		"Custom Server Generator. You can generate with http, https and express(both) modules. As client you can use\n  pure HTML,\n  HTML Preprocessor PugJS,\n  SPA ReactJS,\n  SSR NextJS\n\nAdditional Info at gitlab.com/MDReal/custom-server-generator",
	)
	.command(
		"$0",
		false,
		(yargs) => yargs.options(opts).parse(),
		(argv) => {
			if (argv._.length === 0) return yargs.showHelp();
			if (argv._.length > 1) {
				throw new Error("At now you can create only one app pre usage.");
				// console.log("Creating More than 1 app");
			} else {
				console.log(`Creating ${argv._["0"]} App`.info);
			}
			require("../generate")(argv._["0"], argv);
		},
	)
	.version(version)
	.group(["engine"], "Engines:")
	.group(["next", "react", "pug"], "Client:")
	.group(["electron", "socket"], "Technologies:")
	.group("client", "Settings:")
	.help()
	.parse();
