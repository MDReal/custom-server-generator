module.exports = (socket, react, next, sass, pug, client) => {
	if (next) react = pug = false;
	if (react) pug = false;

	const _module = {
		packages: [],
		devPackages: [],
		files: {},
		folders: [],
		config: { js: {}, json: {} },
	};

	if (next) {
		const next = require("./generators/next")(sass, socket, client);

		_module.packages.push(...next.packages);
		_module.devPackages.push(...next.devPackages);
		_module.files = { ..._module.files, ...next.files };
		_module.folders.push(...next.folders);
		_module.config.json = { ..._module.config.json, ...next.config.json };
		_module.config.js = { ..._module.config.js, ...next.config.js };
	} else if (react) {
		_module.react = {
			packageJSON: {},
			packages: [],
			devPackages: [],
		};
	}

	return _module;
};
