module.exports = (socket, pug, client) => {
	const rtn = {
		files: {},
		folders: [],
	};

	rtn.files[`${client}/index.${pug ? "pug" : "html"}`] = pug
		? `doctype html
html(lang='en')
  head
    title Hello @User
    meta(charset='UTF-8')
  body(style='background: #333;')
    div(style='color: #fff;')
      h1 Hi
      p This is Example Page
      p Thanks for Use My Custom Server Generator.
      p
        | You can see last updates in 
        a(href='https://gitlab.com/MDReal/custom-server-generator#updates') Here
      p
        | You can support me send Reports to 
        a(href='https://gitlab.com/MDReal/custom-server-generator/-/issues') Here
      p This is Open Source Project.
			p And I&apos;m not Designer ))
`
		: `<!DOCTYPE html>
<html lang="en">
<head>
	<title>Hello @User</title>
	<meta charset="UTF-8" />
</head>
<body style="background: #333;">
	<div style="color: #fff;">
		<h1>Hi</h1>
		<p>This is Example Page</p>
		<p>Thanks for Use My Custom Server Generator.</p>
		<p>You can see last updates in <a href="https://gitlab.com/MDReal/custom-server-generator#updates">Here</a></p>
		<p>You can support me send Reports to <a href="https://gitlab.com/MDReal/custom-server-generator/-/issues">Here</a></p>
		<p>This is Open Source Project.</p>
		<p>And I'm not Designer ))</p>
	</div>
${socket ? '\n\t<script src="/socket.io/socket.io.js"></script>' : ""}
</body>
</html>`;

	if (socket) {
		rtn.files[`${client}/Socket/index.js`] = ``;
		rtn.files[`${client}/Socket/SocketCore.js`] = ``;
	}

	return rtn;
};
