module.exports = (sass, socket, client) => {
	const rtn = {
		packages: ["next", "react", "react-dom"],
		devPackages: [],
		files: {},
		folders: [],
		config: { js: {}, json: {} },
	};

	if (sass) rtn.devPackages.push("sass");

	rtn.folders.push(`${client}/Components`);
	rtn.folders.push(`${client}/assets/scripts`);
	rtn.folders.push(`${client}/assets/styles`);
	rtn.folders.push(`${client}/assets/handlers`);
	rtn.folders.push(`${client}/assets/media/svg`);
	rtn.folders.push(`${client}/assets/media/images`);
	rtn.folders.push(`${client}/assets/media/videos`);

	rtn.config.json.jsconfig = JSON.stringify(
		{
			compilerOptions: {
				baseUrl: `${client}`,
				paths: {
					"@comp/*": ["components/*"],
					"@scripts/*": ["assets/js/*"],
					"@styles/*": ["assets/styles/*"],
					"@handlers/*": ["assets/handlers/*"],
					"@svg/*": ["assets/media/svg/*"],
					"@images/*": ["assets/media/images/*"],
					"@videos/*": ["assets/media/videos/*"],
				},
			},
		},
		null,
		2,
	);

	if (socket) rtn.packages.push("socket.io-client");

	rtn.files[`${client}/pages/index.jsx`] = `const Index = ({ hello }) => {
	return <div style="color: #fff;">
		<h1>{hello}</h1>
		<p>This is Example Page</p>
		<p>Thanks for Use My Custom Server Generator.</p>
		<p>You can see last updates in <a href="https://gitlab.com/MDReal/custom-server-generator#updates">Here</a></p>
		<p>You can support me send Reports to <a href="https://gitlab.com/MDReal/custom-server-generator/-/issues">Here</a></p>
		<p>This is Open Source Project.</p>
		<p>And I'm not Designer ))</p>
	</div>
};

export default Index;

Index.getInitialProps = async () => {
	return { hello: "Hi @User" };
};`;

	return rtn;
};
