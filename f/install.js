const spawn = require("cross-spawn");
const { execSync } = require("child_process");

module.exports = (root, dev, dependencies) =>
	new Promise((resolve, reject) => {
		console.log(`Installing packages: ${dependencies.join(", ")}`.info);
		let command, args, useYarn;
		try {
			execSync("yarn version", { stdio: "pipe" });
			useYarn = true;
		} catch (e) {
			useYarn = false;
		}

		if (useYarn) {
			command = "yarnpkg";
			args = ["add", "--exact"];
			[].push.apply(args, dependencies);

			args.push("--cwd", root);
		} else {
			command = "npm";
			args = [
				"install",
				`--save${dev ? "-dev" : ""}`,
				"--save-exact",
				"--loglevel",
				"error",
				"--prefix",
				root,
			].concat(dependencies);
		}

		if (process.env.NODE_ENV !== "development")
			spawn(command, args, { stdio: "inherit" }).on("close", (code) => {
				if (code !== 0) {
					return reject({
						command: `${command} ${args.join(" ")}`,
					});
				}
				resolve();
			});
		else resolve();
	});
