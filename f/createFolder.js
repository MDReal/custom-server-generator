const { mkdirSync } = require("fs");
const { dirname } = require("path");

let createFolder;
module.exports = createFolder = (dir) => {
	try {
		mkdirSync(dir);
	} catch (e) {
		createFolder(dirname(dir));
		mkdirSync(dir);
	}
};
