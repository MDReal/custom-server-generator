Object.defineProperty(String.prototype, "info", {
	get() {
		return this.toString().cyan.italic.bold;
	},
});

Object.defineProperty(String.prototype, "warning", {
	get() {
		return this.toString().yellow.italic.bold;
	},
});

Object.defineProperty(String.prototype, "error", {
	get() {
		return this.toString().red.italic.bold;
	},
});
