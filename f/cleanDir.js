const { resolve } = require("path");
const { rmdirSync, unlinkSync, readdirSync, statSync } = require("fs");

let cleanDir;
module.exports = cleanDir = (dir) => {
	try {
		readdirSync(dir).map((file) => {
			file = resolve(dir, file);
			try {
				unlinkSync(file);
			} catch (e) {
				cleanDir(file);
				rmdirSync(file);
			}
		});
	} catch (e) {}
};
