module.exports = (name, electron) => ({
	name,
	version: "1.0.0",
	description: "",
	main: `${name}.js`,
	private: true,
	server: {
		port: 3500,
	},
	scripts: {
		start: `cross-env NODE_ENV=production ${electron ? "electron" : "node"} ${name}.js`,
		dev: `cross-env NODE_ENV=development nodemon --ignore "./client" --exec "clear; ${
			electron ? "electron" : "node"
		}" ${name}.js`,
	},
	browserslist: [">1%", "not ie 11", "not dead", "not op_mini all"],
	keywords: [name],
	author: "",
	license: "ISC",
});
