const { writeFileSync } = require("fs");
const createFolder = require("./createFolder");
const { dirname } = require("path");

module.exports = (file, content) => {
	try {
		writeFileSync(file, content);
	} catch (e) {
		createFolder(dirname(file));
		writeFileSync(file, content);
	}
};
