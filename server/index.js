module.exports = async (engine, router, electron, socket, next, pug, client, pname) => {
	const _module = { packages: [], devPackages: ["nodemon", "cross-env"], files: {}, folders: [] };

	const index = require("./generators/index")({ engine, electron, socket, next, pug, client });
	_module.packages.push(...index.packages);
	_module.devPackages.push(...index.devPackages);
	_module.files[`${pname}.js`] = index.code;

	if (socket) {
		const socket = require("./generators/Socket")();
		_module.packages.push(...socket.packages);
		_module.devPackages.push(...socket.devPackages);
		_module.files["Socket/SocketCore.js"] = socket.SocketCore;
		_module.files["Socket/index.js"] = socket.index;
	}

	if (engine === "express" || engine === "https") {
		const certs = await require("./generators/certs")();
		_module.files["cert/server.cert"] = certs.cert;
		_module.files["cert/server.key"] = certs.key;
	}

	return _module;
};
