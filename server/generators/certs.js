const pem = require("pem");

module.exports = () =>
	new Promise((res, rej) => {
		pem.createCertificate({ days: 1, selfSigned: true }, function (err, keys) {
			if (err) return rej(err);
			const { serviceKey, certificate } = keys;
			res({ key: serviceKey, cert: certificate });
		});
	});
