const _packages = {
	packages: {
		express: ["express"],
		http: ["http"],
		https: ["https"],
	},
	devPackages: {
		express: [],
		http: [],
		https: [],
	},
};

module.exports = ({ engine, electron, socket, next, pug, client }) => {
	const rtn = {
		packages: ["colors", ..._packages.packages[engine]],
		devPackages: _packages.devPackages[engine],
	};
	const express = engine === "express";
	const https = express || engine === "https";
	const http = express || engine === "http";
	const space = next || electron ? "  " : "";

	if (next) pug = false;
	if (electron) rtn.devPackages.push("electron");

	rtn.code =
		(electron ? `const { app: electronApp, BrowserWindow, screen } = require("electron");\n` : "") +
		(https ? `const { createServer: httpsCreateServer } = require("https");\n` : "") +
		(http ? `const { createServer: httpCreateServer } = require("http");\n` : "") +
		'const packageJSON = require("./package.json");\n' +
		'const { networkInterfaces } = require("os");\n' +
		(https ? `const { readFileSync } = require("fs");\n` : "") +
		(socket ? `const SocketIO = require("socket.io");\n` : "") +
		(https ? `const { resolve } = require("path");\n` : "") +
		(socket ? `const Socket = require("./Socket");\n` : "") +
		(express ? `const express = require("express");\n` : "") +
		(next ? `const next = require("next");\n` : "") +
		"\n" +
		'require("colors")' +
		"\n" +
		(express ? "const app = express();\n\n" : "") +
		(pug ? `app.set("view engine", "pug");\napp.set("views", "./${client}");\n` : "") +
		`app.use(express.static(resolve(__dirname, "./${client}")));\n` +
		(pug ? `app.all('*', (req, res) => res.render('index', { title: 'Hello @User'}));\n` : "") +
		(http || https
			? "\nconst httpPort = process.env.PORT || packageJSON.server.port || 3500;\n"
			: "") +
		(https ? "const httpsPort = httpPort + 43;\n" : "") +
		`const localNetwork = networkInterfaces()
  .enp2s0f1.map((e) => e.address)
	.filter((e) => e.match(/^[\\d\\.]+$/))["0"];\n` +
		(http ? `\nconst httpServer = httpCreateServer(app);${https ? "" : "\n"}` : "") +
		(https
			? `\nconst httpsServer = httpsCreateServer(
	{
		key: readFileSync(resolve(__dirname, "cert/server.key"), "utf8"),
		cert: readFileSync(resolve(__dirname, "cert/server.cert"), "utf8"),
	},
	app,
);\n`
			: "") +
		(next
			? `\nconst isProd = process.env.NODE_ENV === "production";
const isDev = !isProd;
const nextApp = next({ dev: isDev, dir: "./${client}" });
const handle = nextApp.getRequestHandler();\n`
			: "") +
		(next || electron ? "\n(async () => {" : "") +
		(next ? `\n${space}await nextApp.prepare();\n${space}app.all("*", handle);\n\n` : "") +
		(http
			? `${space}httpServer.listen(httpPort, (port = httpPort) => {\n${space}  const mess = \`\${\`http\`.bold}  server:  \${\`http://\${localNetwork}:\${port}\`.green.bold}\`;\n${space}  const localmess = \`http://localhost:\${port}\`.green.bold;\n${space}  console.log(\n${space}  	\`\${mess} or\\n\${" ".repeat(mess.length - localmess.length - "".bold.length)}\${localmess}\`,\n${space}  );\n${space}});\n`
			: "") +
		(https
			? `${space}httpsServer.listen(httpsPort, (port = httpsPort) => {\n${space}  const mess = \`\${\`https\`.bold}  server:  \${\`https://\${localNetwork}:\${port}\`.green.bold}\`;\n${space}  const localmess = \`https://localhost:\${port}\`.green.bold;\n${space}  console.log(\n${space}   \`\${mess} or\\n\${" ".repeat(mess.length - localmess.length - "".bold.length)}\${localmess}\`,\n${space}  );\n${space}});\n`
			: "") +
		(electron
			? `\n${space}await electronApp.whenReady();\n${space}const { width, height } = screen.getPrimaryDisplay().workAreaSize;\n${space}const window = new BrowserWindow({\n${space}  width: (width * 3) / 5,\n${space}  height: (height * 3) / 5,\n${space}  webPreferences: {\n${space}    nodeIntegration: true,\n${space}  },\n${space}});\n${space}window.loadURL(\`http://localhost:\${httpPort}\`);\n`
			: "") +
		(http && socket ? `\n${space}new Socket(SocketIO(httpServer));\n` : "") +
		(https && socket ? `${space}new Socket(SocketIO(httpsServer));\n` : "") +
		(next || electron ? "})()" : "") +
		"\n";

	return rtn;
};
