module.exports = () => {
	const rtn = {
		packages: [],
		devPackages: [],
		index: "",
		SocketCore: "",
	};

	rtn.packages.push("socket.io");

	rtn.index = `const SocketCore = require("./SocketCore");

module.exports = class Socket {
	constructor(io) {
		this.io = new SocketCore(io);
	}
};
`;

	rtn.SocketCore = `class SocketCore {
  constructor(io) {
    this.io = io
  }

  on(code, f = null) {
    return new Promise((resolve) => {
      this.io.on(code, (data) => {
        if (f) this.io.emit(\`\${code}:done\`, f(data))
        else
          resolve({
            data,
            handler: (data) => {
              this.io.emit(\`\${code}:done\`, data)
            },
          })
      })
    })
  }

  emit(code, data, f) {
    return new Promise((resolve) => {
      this.io.emit(code, data)
      this.io.on(\`\${code}:reiceved\`, (rdata) => {
        if (f) f(rdata)
        else resolve(rdata)
      })
    })
  }
}

module.exports = SocketCore
`;

	return rtn;
};
